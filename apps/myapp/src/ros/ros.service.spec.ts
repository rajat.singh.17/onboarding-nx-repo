import { Test, TestingModule } from '@nestjs/testing';
import { RosService } from './ros.service';

describe('RosService', () => {
  let service: RosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RosService],
    }).compile();

    service = module.get<RosService>(RosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
