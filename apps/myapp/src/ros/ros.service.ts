import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import * as ROSLIB from 'roslib';

const ros = new ROSLIB.Ros({
    url: 'wss://dev.flytbase.com/websocket',
});

let auth=false

console.log("service triggered")

ros.on('connection', (id)=>{
  console.log('Connected to ROS Web Socket Server');
  auth=true
})

let namespaceGlobal:string;

@Injectable()
export class RosService implements ROSLIB {

    myAuthObs = new Observable((obs) => {
      const authService = new ROSLIB.Service({
        ros: ros,
        name: '/websocket_auth',
        serviceType:'connection'
      });

      const authRequest = new  ROSLIB.ServiceRequest({
        vehicleid:'EcmA1Qft',
        authorization: 'Token 856e06e3b3c353e72f20166153a430963bb8a970'
      });
  
      const namespaceService = new ROSLIB.Service({
        ros : ros,
        name : '/get_global_namespace',
        serviceType : 'core_api/ParamGetGlobalNamespace'
      });
    
      const namespaceRequest = new ROSLIB.ServiceRequest({});
  
      if(auth){
        authService.callService(authRequest, (result) => {
          if (result.success) {
            console.log('Success on auth >>>>>', result);
  
            namespaceService.callService(namespaceRequest,   function(result) {
              if(result.success){
                const ns =  result.param_info.param_value;
                console.log('Namespace got as:',ns);
                namespaceGlobal=ns
              }
              else{
                console.log('Error: Not able to get namespace')
              }
              
              // takeOff()
              // globalPsn = globalPosition()
              // land()
              obs.next(namespaceGlobal)
              
            })
              
          }
          else{
            console.log('Auth failed',result);
          }
          
        })
      }
    });
    


    dronePositionObs = new Observable((obs) => {
        const gpsData = new ROSLIB.Topic({
            ros: ros,
            name : '/'+namespaceGlobal+'/mavros/global_position/global',
            messageType : 'sensor_msgs/NavSatFix'
        })

        console.log('gpsData',gpsData)
        
        gpsData.subscribe(function(message) {
            obs.next(message);
        });

        obs.next();
    })




    takeOffObs = new Observable((obs)=>{
        console.log('myTakeOffObs triggered')
        const takeoff = new ROSLIB.Service({
            ros: ros,
            name: '/'+namespaceGlobal+'/navigation/take_off',
            serviceType: 'core_api/TakeOff'
        })
        
        console.log('takeoff',takeoff)

        const takeoffRequest = new ROSLIB.ServiceRequest({
            takeoff_alt:5.00
        })
        
        takeoff.callService(takeoffRequest,function(result){
            console.log('Result of Takeoff Service call on '
            + takeoff.name
            + ': '
            + result.success
            + ': '
            + result.message);
            obs.next()
        })
    })



    landObs = new Observable((obs)=>{
        console.log('landObs triggered')
        const land = new ROSLIB.Service({
            ros : ros,
            name : '/'+namespaceGlobal+'/navigation/land',
            serviceType : 'core_api/Land'
        });
        
        const landRequest = new ROSLIB.ServiceRequest({});
        
        land.callService(landRequest, function(result) {
            console.log('Result for service call on '
                + land.name
                + ': '
                + result.success
                +': '
                + result.message);
        });
    })


    
    setVelocityObs = new Observable((obs)=>{
        console.log('setVelocityObs triggered')
        const velocitySet = new ROSLIB.Service({
            ros : ros,
            name : '/'+namespaceGlobal+'/navigation/velocity_set',
            serviceType : 'core_api/VelocitySet'
        });
        
        const request = new ROSLIB.ServiceRequest({
            vx: 5.00,
            vy: 3.00,
            vz: -0.00,
            yaw_rate: 1.00,
            tolerance: 2.00,
            async: true,
            relative: false,
            yaw_rate_valid : true,
            body_frame : false
        });
        
        velocitySet.callService(request, function(result) {
            console.log('Result for service call on '
                + velocitySet.name
                + ': '
                + result.success
                +': '
                + result.message);
        });
    })
}
