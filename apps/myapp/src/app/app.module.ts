import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppGateway } from './app.gateway';
import { RosService } from '../ros/ros.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppGateway,AppService,RosService],
})
export class AppModule {}
