import { OnGatewayConnection, SubscribeMessage, WebSocketGateway, WebSocketServer } from "@nestjs/websockets";
import { Server, Socket } from 'socket.io';
import { RosService } from "../ros/ros.service";



@WebSocketGateway()
export class AppGateway implements OnGatewayConnection {

    private posiSubs

    @WebSocketServer() server: Server;


    constructor(private RosService:RosService) {
        this.server = new Server();
        console.log('WebSocket server started on port 8080');
    }

    handleConnection(socket: any) {
        console.log('New client connected',socket.id);
    }

    afterInit(server: any) {
        console.log('WebSocket gateway initialized!')
    }

    @SubscribeMessage('Auth')
    handleMessage(socket: Socket, payload: any): void {
        console.log('Received message:',payload );
        this.RosService.myAuthObs.subscribe(data =>{
            console.log('data from auth service: ',data)
            socket.emit('Auth','Auth Successful. Namespace got as: '+data)
        })
        // this.server.emit('drone','this is broadcast message')
    }

    @SubscribeMessage('PositionSub')
    handlePositionMessage(socket: Socket){
        this.posiSubs=this.RosService.dronePositionObs.subscribe(data =>{
            socket.emit('Position',data);
        })
    }

    @SubscribeMessage('PositionUnsub')
    handleUnsubPositionMessage(socket: Socket){
        this.posiSubs.unsubscribe();
    }
}